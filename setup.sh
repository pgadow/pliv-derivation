# setupATLAS -q

export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
source /cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/user/atlasLocalSetup.sh -q
## build ##
mkdir -p build && cd build
# set up release
asetup Athena,25.0.12
# asetup Athena,main,latest
cmake ../athena/Projects/WorkDir/
make
source x*/setup.sh
## compile to do a local test, to make sure the change works ##
cd ../ && mkdir -p run && cd run
## then here in the run directory to perform a local test
cd ../
