#!/bin/bash

# EOS xAOD file
# AOD_FILE=/eos/user/p/pgadow/data/aod/mc20_13TeV.410470.PhPy8EG_A14_ttbar_hdamp258p75_nonallhad.recon.AOD.e6337_s3681_r13144/AOD.27120913._001022.pool.root.1
# Chicago AF xAOD file
AOD_FILE=/data/pgadow/atlas/pliv/xaod/mc20_13TeV.410470.PhPy8EG_A14_ttbar_hdamp258p75_nonallhad.recon.AOD.e6337_s3681_r13144/AOD.27120913._000822.pool.root.1

cd run/
rm *
Derivation_tf.py --CA \
    --formats PHYS \
    --inputAODFile $AOD_FILE \
    --maxEvents 100 \
    --outputDAODFile test.root
ls
# checkFile -d DAOD_PHYS.test.root
cd -
