#!/bin/bash

# EOS xAOD file
# AOD_FILE=/eos/user/p/pgadow/data/aod/mc20_13TeV.410470.PhPy8EG_A14_ttbar_hdamp258p75_nonallhad.recon.AOD.e6337_s3681_r13144/AOD.27120913._001022.pool.root.1

# Chicago AF xAOD file
# AOD_FILE=/data/pgadow/atlas/plit/xaod/mc20_13TeV.410470.PhPy8EG_A14_ttbar_hdamp258p75_nonallhad.recon.AOD.e6337_s3681_r13144/AOD.27120913._000822.pool.root.1

# NAF xAOD file
AOD_FILE=/nfs/dust/atlas/user/pgadow/plit/data/xaod/mc20_13TeV.410470.PhPy8EG_A14_ttbar_hdamp258p75_nonallhad.recon.AOD.e6337_s3681_r13144/AOD.27231746._000774.pool.root.1

mkdir -p run_MUON1 && cd run_MUON1/
rm *
Derivation_tf.py --CA \
    --formats MUON1 \
    --inputAODFile $AOD_FILE \
    --maxEvents 1000 \
    --postExec "cfg.getService('MessageSvc').Format = \"S:%s E:%e % F%128W%S%7W%R%T  %0W%M\"" \
    --outputDAODFile test.root
    # --athenaopts="--debug=exec" \
ls
# checkFile -d DAOD_MUON1.test.root
cd -
