#!/bin/bash
# setup atlas software
# setupATLAS -q
lsetup git
git atlas init-workdir ssh://git@gitlab.cern.ch:7999/atlas/athena.git -b main
cd athena
git checkout main-leptontaggers upstream/main --no-track # to checkout a new branch for later making MRs
git atlas addpkg DerivationFrameworkPhys DerivationFrameworkMuons LeptonTaggers FlavorTagDiscriminants

  # or other packages you want

# copy lepton taggers
# cp -r ../src/LeptonTaggers/ PhysicsAnalysis/AnalysisCommon/LeptonTaggers/

## make changes ##
cd ../ && mkdir -p build && cd build
asetup Athena,25.0.6
cmake ../athena/Projects/WorkDir/
make 
source x*/setup.sh
## compile to do a local test, to make sure the change works ##
cd ../ && mkdir -p run && cd run
## then here in the run directory to perform a local test
